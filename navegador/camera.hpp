#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

class OpenGLWindow;

class Camera {
 public:
  void computeViewMatrix();
  void computeProjectionMatrix(int width, int height);

  void cameraZoom(float speed);
  void cameraMovelLateral(float speed);
  void cameraEstaticaLateral(float speed);
  void cameraMovelVertical(float speed);
  float angleBetween(glm::vec3 a, glm::vec3 b, glm::vec3 origin);
  void cameraEstaticaVertical(float speed);
  void centerBarco(glm::vec3 position,float angle);

 private:
  friend OpenGLWindow;

  glm::vec3 m_eye{glm::vec3(0.0f, 0.5f, 2.5f)};  
  glm::vec3 m_at{glm::vec3(0.0f, 0.5f, 0.0f)};  
  glm::vec3 m_up{glm::vec3(0.0f, 1.0f, 0.0f)}; 

  glm::mat4 m_viewMatrix{1.0f};
  glm::mat4 m_projMatrix{1.0f};

  bool m_livre{true};

};

#endif