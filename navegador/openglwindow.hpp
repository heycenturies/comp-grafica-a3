#ifndef OPENGLWINDOW_HPP_
#define OPENGLWINDOW_HPP_

#include <string_view>
#include "abcg.hpp"
#include "camera.hpp"
#include "model.hpp"
#include "barco.hpp"

class OpenGLWindow : public abcg::OpenGLWindow {
 protected:
  void handleEvent(SDL_Event& ev) override;
  void initializeGL() override;
  void paintGL() override;
  void paintUI() override;
  void resizeGL(int width, int height) override;
  void terminateGL() override;

 private:
  GLuint m_program{};

  int m_viewportWidth{};
  int m_viewportHeight{};

  Model m_model;
  Model m_modelObj1;
  Model m_modelFloor;
  Camera m_camera;
  Barco m_barco;
  float m_cameraZoomSpeed{0.0f};
  float m_cameraMovelSpeed{0.0f};
  float m_cameraEstaticaSpeed{0.0f};
  float m_cameraMovelVerticalSpeed{0.0f};
  float m_cameraEstaticaVerticalSpeed{0.0f};

  // Light and material properties
  glm::vec4 m_lightDir{-1.0f, -1.0f, -1.0f, 0.0f};
  glm::vec4 m_Ia{1.0f, 1.0f, 1.0f, 1.0f};
  glm::vec4 m_Id{1.0f, 1.0f, 1.0f, 1.0f};
  glm::vec4 m_Is{1.0f, 1.0f, 1.0f, 1.0f};
  
  // Skybox
  const std::string m_skyShaderName{"skybox"};
  GLuint m_skyVAO{};
  GLuint m_skyVBO{};
  GLuint m_skyProgram{};

  // clang-format off
  const std::array<glm::vec3, 36>  m_skyPositions{
    // Front
    glm::vec3{-1000, -1000, +1000}, glm::vec3{+1000, -1000, +1000}, glm::vec3{+1000, +1000, +1000},
    glm::vec3{-1000, -1000, +1000}, glm::vec3{+1000, +1000, +1000}, glm::vec3{-1000, +1000, +1000},
    // Back
    glm::vec3{+1000, -1000, -1000}, glm::vec3{-1000, -1000, -1000}, glm::vec3{-1000, +1000, -1000},
    glm::vec3{+1000, -1000, -1000}, glm::vec3{-1000, +1000, -1000}, glm::vec3{+1000, +1000, -1000},
    // Right
    glm::vec3{+1000, -1000, -1000}, glm::vec3{+1000, +1000, -1000}, glm::vec3{+1000, +1000, +1000},
    glm::vec3{+1000, -1000, -1000}, glm::vec3{+1000, +1000, +1000}, glm::vec3{+1000, -1000, +1000},
    // Left
    glm::vec3{-1000, -1000, +1000}, glm::vec3{-1000, +1000, +1000}, glm::vec3{-1000, +1000, -1000},
    glm::vec3{-1000, -1000, +1000}, glm::vec3{-1000, +1000, -1000}, glm::vec3{-1000, -1000, -1000},
    // Top
    glm::vec3{-1000, +1000, +1000}, glm::vec3{+1000, +1000, +1000}, glm::vec3{+1000, +1000, -1000},
    glm::vec3{-1000, +1000, +1000}, glm::vec3{+1000, +1000, -1000}, glm::vec3{-1000, +1000, -1000},
    // Bottom
    glm::vec3{-1000, -1000, -1000}, glm::vec3{+1000, -1000, -1000}, glm::vec3{+1000, -1000, +1000},
    glm::vec3{-1000, -1000, -1000}, glm::vec3{+1000, -1000, +1000}, glm::vec3{-1000, -1000, +1000}
  };


  void initializeSkybox();
  void renderSkybox();
  void terminateSkybox();
  void update();
  
};

#endif