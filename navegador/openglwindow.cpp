#include "openglwindow.hpp"

#include <fmt/core.h>
#include <imgui.h>

#include <cppitertools/itertools.hpp>
#include <glm/gtc/matrix_inverse.hpp>

void OpenGLWindow::handleEvent(SDL_Event& ev) {
  if (ev.type == SDL_KEYDOWN) {
    if (ev.key.keysym.sym ==SDLK_z)      m_cameraZoomSpeed = 1.0f;
    if (ev.key.keysym.sym == SDLK_x)     m_cameraZoomSpeed = -1.0f;
    if (ev.key.keysym.sym == SDLK_LEFT)  m_cameraEstaticaSpeed = -1.0f;
    if (ev.key.keysym.sym == SDLK_RIGHT) m_cameraEstaticaSpeed = 1.0f;
    if (ev.key.keysym.sym == SDLK_UP)    m_cameraEstaticaVerticalSpeed = 1.0f;
    if (ev.key.keysym.sym == SDLK_DOWN)  m_cameraEstaticaVerticalSpeed = -1.0f;
    if (ev.key.keysym.sym == SDLK_w)     m_barco.m_speed = -1.0f;
    if (ev.key.keysym.sym == SDLK_s)     m_barco.m_speed = 1.0f;
    if (ev.key.keysym.sym == SDLK_a)     m_barco.m_side = -1.0f;
    if (ev.key.keysym.sym == SDLK_d)     m_barco.m_side = 1.0f;
    if (ev.key.keysym.sym == SDLK_j)     m_cameraMovelSpeed = -1.0f;
    if (ev.key.keysym.sym == SDLK_l)     m_cameraMovelSpeed = 1.0f;
    if (ev.key.keysym.sym == SDLK_k)     m_cameraMovelVerticalSpeed = -1.0f;
    if (ev.key.keysym.sym == SDLK_i)     m_cameraMovelVerticalSpeed = 1.0f;
  }
  if (ev.type == SDL_KEYUP) {
    if ((ev.key.keysym.sym == SDLK_z) && m_cameraZoomSpeed > 0)      m_cameraZoomSpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_x) && m_cameraZoomSpeed < 0)      m_cameraZoomSpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_LEFT) && m_cameraEstaticaSpeed < 0)     m_cameraEstaticaSpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_RIGHT) && m_cameraEstaticaSpeed > 0)    m_cameraEstaticaSpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_UP) && m_cameraEstaticaVerticalSpeed > 0)   m_cameraEstaticaVerticalSpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_DOWN) && m_cameraEstaticaVerticalSpeed < 0) m_cameraEstaticaVerticalSpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_w) && m_barco.m_speed < 0)       m_barco.m_speed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_s) && m_barco.m_speed > 0)       m_barco.m_speed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_a) && m_barco.m_side < 0)     m_barco.m_side = 0.0f;
    if ((ev.key.keysym.sym == SDLK_d) && m_barco.m_side > 0)    m_barco.m_side = 0.0f;
    if ((ev.key.keysym.sym == SDLK_j) && m_cameraMovelSpeed < 0) m_cameraMovelSpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_l) && m_cameraMovelSpeed > 0) m_cameraMovelSpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_k) && m_cameraMovelVerticalSpeed < 0) m_cameraMovelVerticalSpeed = 0.0f;
    if ((ev.key.keysym.sym == SDLK_i) && m_cameraMovelVerticalSpeed > 0) m_cameraMovelVerticalSpeed = 0.0f;
  }
}

void OpenGLWindow::initializeGL() {
  abcg::glClearColor(0, 20, 200, 1);
  abcg::glEnable(GL_DEPTH_TEST);
  
  m_program = createProgramFromFile(getAssetsPath() + "shaders/texture.vert",
                                    getAssetsPath() + "shaders/texture.frag");

  
  m_model.loadDiffuseTexture(getAssetsPath() + "maps/metal.jpg");
  m_modelFloor.loadDiffuseTexture(getAssetsPath() + "maps/mar.jpg");
  m_modelObj1.loadDiffuseTexture(getAssetsPath() + "maps/bricks.jpg");

  m_model.loadFromFile(getAssetsPath() + "barco.obj");
  m_modelObj1.loadFromFile(getAssetsPath() + "mirante.obj");
  m_modelFloor.loadFromFile(getAssetsPath() + "oceano.obj");
  
  m_model.setupVAO(m_program);
  m_modelObj1.setupVAO(m_program);
  m_modelFloor.setupVAO(m_program);
  resizeGL(getWindowSettings().width, getWindowSettings().height);

  m_model.loadCubeTexture(getAssetsPath() + "maps/cube/");
  initializeSkybox();
}

void OpenGLWindow::initializeSkybox() {
  // Create skybox program
  const auto path{getAssetsPath() + "shaders/" + m_skyShaderName};
  m_skyProgram = createProgramFromFile(path + ".vert", path + ".frag");

  // Generate VBO
  abcg::glGenBuffers(1, &m_skyVBO);
  abcg::glBindBuffer(GL_ARRAY_BUFFER, m_skyVBO);
  abcg::glBufferData(GL_ARRAY_BUFFER, sizeof(m_skyPositions),
                     m_skyPositions.data(), GL_STATIC_DRAW);
  abcg::glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Get location of attributes in the program
  const GLint positionAttribute{
      abcg::glGetAttribLocation(m_skyProgram, "inPosition")};

  // Create VAO
  abcg::glGenVertexArrays(1, &m_skyVAO);

  // Bind vertex attributes to current VAO
  abcg::glBindVertexArray(m_skyVAO);

  abcg::glBindBuffer(GL_ARRAY_BUFFER, m_skyVBO);
  abcg::glEnableVertexAttribArray(positionAttribute);
  abcg::glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, GL_FALSE, 0,
                              nullptr);
  abcg::glBindBuffer(GL_ARRAY_BUFFER, 0);

  // End of binding to current VAO
  abcg::glBindVertexArray(0);
}


void OpenGLWindow::paintGL() {
  update();

  // Clear color buffer and depth buffer
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glViewport(0, 0, m_viewportWidth, m_viewportHeight);

  glUseProgram(m_program);

  GLint viewMatrixLoc{glGetUniformLocation(m_program, "viewMatrix")};
  GLint projMatrixLoc{glGetUniformLocation(m_program, "projMatrix")};
  GLint modelMatrixLoc{glGetUniformLocation(m_program, "modelMatrix")};
  GLint normalMatrixLoc{glGetUniformLocation(m_program, "normalMatrix")};
  GLint lightDirLoc{glGetUniformLocation(m_program, "lightDirWorldSpace")};
  GLint shininessLoc{glGetUniformLocation(m_program, "shininess")};
  GLint IaLoc{glGetUniformLocation(m_program, "Ia")};
  GLint IdLoc{glGetUniformLocation(m_program, "Id")};
  GLint IsLoc{glGetUniformLocation(m_program, "Is")};
  GLint KaLoc{glGetUniformLocation(m_program, "Ka")};
  GLint KdLoc{glGetUniformLocation(m_program, "Kd")};
  GLint KsLoc{glGetUniformLocation(m_program, "Ks")};
  GLint diffuseTexLoc{glGetUniformLocation(m_program, "diffuseTex")};
  GLint mappingModeLoc{glGetUniformLocation(m_program, "mappingMode")};

  glUniform4fv(lightDirLoc, 1, &m_lightDir.x);
  glUniform4fv(IaLoc, 1, &m_Ia.x);
  glUniform4fv(IdLoc, 1, &m_Id.x);
  glUniform4fv(IsLoc, 1, &m_Is.x);
  glUniform1i(diffuseTexLoc, 0);
  
  glUniformMatrix4fv(viewMatrixLoc, 1, GL_FALSE, &m_camera.m_viewMatrix[0][0]);
  glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, &m_camera.m_projMatrix[0][0]);
  

  glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, &m_barco.m_modelMatrix[0][0]);
  
  auto modelViewMatrix{glm::mat3(m_camera.m_viewMatrix * m_barco.m_modelMatrix)};
  glm::mat3 normalMatrix{glm::inverseTranspose(modelViewMatrix)};
  glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, &normalMatrix[0][0]);

  glUniform1f(shininessLoc, m_model.m_shininess);
  glUniform4fv(KaLoc, 1, &m_model.m_Ka.x);
  glUniform4fv(KdLoc, 1, &m_model.m_Kd.x);
  glUniform4fv(KsLoc, 1, &m_model.m_Ks.x);

  glUniform1i(mappingModeLoc, 3);
  m_model.render();


  glm::mat4 mirante{1.0f};
  mirante = glm::scale(mirante, glm::vec3(0.05f));
  glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE,&mirante[0][0]);

  auto modelViewMatrix2{glm::mat3(m_camera.m_viewMatrix * mirante)};
  glm::mat3 normalMatrix2{glm::inverseTranspose(modelViewMatrix2)};
  glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, &normalMatrix2[0][0]);

  glUniform1f(shininessLoc, m_modelObj1.m_shininess);
  glUniform4fv(KaLoc, 1, &m_modelObj1.m_Ka.x);
  glUniform4fv(KdLoc, 1, &m_modelObj1.m_Kd.x);
  glUniform4fv(KsLoc, 1, &m_modelObj1.m_Ks.x);

  glUniform1i(mappingModeLoc, 0);

  m_modelObj1.render();


  glm::mat4 floorModel{1.0f};
  glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE,&floorModel[0][0]);
  glUniform1f(shininessLoc, m_modelFloor.m_shininess);
  glUniform4fv(KaLoc, 1, &m_modelFloor.m_Ka.x);
  glUniform4fv(KdLoc, 1, &m_modelFloor.m_Kd.x);
  glUniform4fv(KsLoc, 1, &m_modelFloor.m_Ks.x);

  glUniform1i(mappingModeLoc, 0);

  //m_modelFloor.render();

  glUseProgram(0);
  renderSkybox();
}

void OpenGLWindow::renderSkybox() {
  abcg::glUseProgram(m_skyProgram);

  // Get location of uniform variables
  const GLint viewMatrixLoc{
      abcg::glGetUniformLocation(m_skyProgram, "viewMatrix")};
  const GLint projMatrixLoc{
      abcg::glGetUniformLocation(m_skyProgram, "projMatrix")};
  const GLint skyTexLoc{abcg::glGetUniformLocation(m_skyProgram, "skyTex")};

  // Set uniform variables
  //const auto viewMatrix{m_trackBallLight.getRotation()};
  
  abcg::glUniformMatrix4fv(viewMatrixLoc, 1, GL_FALSE, &m_camera.m_viewMatrix[0][0]);
  abcg::glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, &m_camera.m_projMatrix[0][0]);
  abcg::glUniform1i(skyTexLoc, 0);

  abcg::glBindVertexArray(m_skyVAO);

  abcg::glActiveTexture(GL_TEXTURE0);
  abcg::glBindTexture(GL_TEXTURE_CUBE_MAP, m_model.getCubeTexture());

  //abcg::glEnable(GL_CULL_FACE); //se copmentar aparece o chao
  //abcg::glFrontFace(GL_CW);
  abcg::glDepthFunc(GL_LEQUAL);
  abcg::glDrawArrays(GL_TRIANGLES, 0, m_skyPositions.size());
  abcg::glDepthFunc(GL_LESS);

  abcg::glBindVertexArray(0);
  abcg::glUseProgram(0);
}



void OpenGLWindow::paintUI() { 
  abcg::OpenGLWindow::paintUI(); 
  auto widgetSize{ImVec2(250, 40)};
  ImGui::SetNextWindowPos(ImVec2(m_viewportWidth - widgetSize.x - 5, 5));
  ImGui::SetNextWindowSize(widgetSize);
  ImGui::Begin("Camera Select", nullptr, ImGuiWindowFlags_NoDecoration);
  {
    static std::size_t currentIndex{};
    std::vector<std::string> comboItems{"Visao traseira","Camera Livre"};

    ImGui::PushItemWidth(180);
    if (ImGui::BeginCombo("Camera",
                          comboItems.at(currentIndex).c_str())) {
      for (auto index : iter::range(comboItems.size())) {
        const bool isSelected{currentIndex == index};
        if (ImGui::Selectable(comboItems.at(index).c_str(), isSelected))
          currentIndex = index;
        if (isSelected) ImGui::SetItemDefaultFocus();
      }
      ImGui::EndCombo();
    }
    ImGui::PopItemWidth();
    if (currentIndex == 0) {
      m_camera.m_livre = false;
    } else {
      m_camera.m_livre = true;
    }
  }


  ImGui::End();
    
}

void OpenGLWindow::resizeGL(int width, int height) {
  m_viewportWidth = width;
  m_viewportHeight = height;
  m_camera.computeProjectionMatrix(width, height);
}

void OpenGLWindow::terminateGL() {
  glDeleteProgram(m_program);
  terminateSkybox();
}

void OpenGLWindow::terminateSkybox() {
  abcg::glDeleteProgram(m_skyProgram);
  abcg::glDeleteBuffers(1, &m_skyVBO);
  abcg::glDeleteVertexArrays(1, &m_skyVAO);
}

void OpenGLWindow::update() {
  float deltaTime{static_cast<float>(getDeltaTime())};
  deltaTime = deltaTime*2.5f;
  if(m_camera.m_livre){
    m_camera.cameraZoom(m_cameraZoomSpeed * deltaTime);
    m_camera.cameraMovelLateral(m_cameraMovelSpeed * deltaTime);
    m_camera.cameraMovelVertical(m_cameraMovelVerticalSpeed * deltaTime);
    m_camera.cameraEstaticaLateral(m_cameraEstaticaSpeed * deltaTime);
    m_camera.cameraEstaticaVertical(m_cameraEstaticaVerticalSpeed * deltaTime);
  }
  else{
    m_camera.centerBarco(m_barco.m_position,m_barco.m_angle);
  }

  m_barco.moveBarco(m_barco.m_speed * deltaTime,m_barco.m_side * deltaTime);

}

