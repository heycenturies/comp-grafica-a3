#ifndef BARCO_HPP_
#define BARCO_HPP_

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

class OpenGLWindow;

class Barco { 
 
 private:
  friend OpenGLWindow;
  glm::mat4 m_modelMatrix{1.0f};
  float m_angle{0.0f};
  float m_speed{0.0f};
  float m_side{0.0f};
  //glm::vec3 m_position{7.6f,0.0f,-1.85f}; // posicao do mirante
  glm::vec3 m_position{12.0f,0.0f,-1.85f};

  void moveBarco(float speed,float side);

};

#endif