#include "barco.hpp"
#include <cppitertools/itertools.hpp>
#include <glm/gtx/fast_trigonometry.hpp>
#include <glm/gtx/hash.hpp>
#include <unordered_map>

//atualiza a posicao do barco com base na velocidade a ajusta o angulo em caso de curva
void Barco::moveBarco(float speed,float side){
    if(speed>0){
        if(side!=0){
            m_angle+=(side*30);
        }
        m_position += glm::vec3(speed*glm::cos(glm::radians(m_angle)),0.0f,-speed*glm::sin(glm::radians(m_angle)));
    }
    if(speed<0){
        if(side!=0){
            m_angle-=(side*30);
        }
        m_position += glm::vec3(speed*glm::cos(glm::radians(m_angle)),0.0f,-speed*glm::sin(glm::radians(m_angle)));
    }

    float borda{0.2f*(1.0f-(static_cast<float>(glm::abs(glm::cos(glm::radians(m_angle)-asin((glm::normalize(m_position-glm::vec3{7.6f,0.0f,-1.85f})).x) ) ) ) ) ) };
    float distance2{glm::distance(m_position,glm::vec3{7.6f,0.0f,-1.85f})};
    if(distance2 < (1.0f+borda)){
        m_position = m_position + (1.01f+borda)*glm::normalize(m_position-glm::vec3{7.6f,0.0f,-1.85f});
    }


    glm::mat4 model{1.0f};
    model = glm::translate(model, m_position);
    model = glm::rotate(model, glm::radians(m_angle), glm::vec3(0, 1, 0));
    model = glm::rotate(model, glm::radians(72.0f), glm::vec3(0, 0, 1));
    model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0, 1, 0));
    model = glm::scale(model, glm::vec3(0.03f));
    m_modelMatrix = model;
}   
